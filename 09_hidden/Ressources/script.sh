#!/bin/sh

# $1 need to be host (ip)

wget -rp -e robots=off --no-parent http://$1/.hidden/
# -r for recursive
# -p for all ressources
# -e for robots=off work
# robots=off for bypass robots.txt restrictions (/.hidden)
# --no-parent for not ascend to parent directory while downloading recursivly.

for dir in $1/.hidden/*
do
cat $dir/*/*/README | grep -v "Demande à ton voisin" | grep -v "Toujours pas tu vas craquer non ?" | grep -v "Tu veux de l'aide ? Moi aussi !" | grep -v "Non ce n'est toujours pas bon ..."
done
